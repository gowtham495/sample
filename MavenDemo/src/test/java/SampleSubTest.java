package test.java;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import junit.framework.Assert;
import main.java.Sample;

@SuppressWarnings("deprecation")
public class SampleSubTest {

	
	@Test
	public void test1() {
		int a=4;
		int b=2;
		Sample obj = new Sample();
		Assert.assertEquals(2, obj.subtraction(a, b));
	}
	
	   @BeforeClass
	   public static void beforeClass() {
	      System.out.println("in before class");
	   }

	   //execute only once, in the end
	   @AfterClass
	   public static void  afterClass() {
	      System.out.println("in after class");
	   }

	   //execute for each test, before executing test
	   @Before
	   public void before() {
	      System.out.println("in before");
	   }
		
	   //execute for each test, after executing test
	   @After
	   public void after() {
	      System.out.println("in after");
	   }


	
	
}
