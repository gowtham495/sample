package test.java;

import org.junit.Test;

import junit.framework.Assert;
import main.java.Sample;

@SuppressWarnings("deprecation")
public class SampleMultiTest {

	@Test
	public void test() {
		int a=2;
		int b=6;
		Sample obj = new Sample();
		Assert.assertEquals(12, obj.multiplication(a, b));
	}

}
