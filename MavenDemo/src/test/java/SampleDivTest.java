package test.java;

import org.junit.Test;

import junit.framework.Assert;
import main.java.Sample;

@SuppressWarnings("deprecation")
public class SampleDivTest {

	@Test
	public void test() {
		int a=6;
		int b=2;
		Sample obj = new Sample();
		Assert.assertEquals(3, obj.division(a, b));
	}
}
