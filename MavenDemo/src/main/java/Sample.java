package main.java;

public class Sample {

	public int addition(int a, int b){                //add
		return a+b;
	}
	public int subtraction(int a, int b){             //sub
		return a-b;
	}
	public int multiplication(int a, int b){          //multiply
		return a*b;
	}
	public int division(int a, int b){                //Divide
		return a/b;
	}
	
	public static void main(String[] args) {
		
		System.out.println("Sample arithmetic program");
	}

}
